## 0.0.6

- Add const to error_result.dart, value_result.dart
- Print toString by async_lite_config.dart

## 0.0.5

* Remove from static _map completed AsyncCache

## 0.0.4

* Added package
