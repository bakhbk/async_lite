part of '../../async_lite.dart';

final class ValueResult<T> implements Result<T> {
  final T value;

  @override
  bool get isValue => true;

  @override
  bool get isError => false;

  @override
  ValueResult<T> get asValue => this;

  @override
  ErrorResult? get asError => null;

  const ValueResult(this.value);

  @override
  Future<T> get asFuture => Future.value(value);

  @override
  int get hashCode => value.hashCode ^ 0x323f1d61;

  @override
  bool operator ==(Object other) =>
      other is ValueResult && value == other.value;

  @override
  String toString() {
    if (!AsyncLiteConfig.printLog) return '$runtimeType';
    return 'ValueResult{value: $value}';
  }
}
