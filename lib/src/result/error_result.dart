part of '../../async_lite.dart';

typedef ZoneBinaryCallback<R, T1, T2> = R Function(T1, T2);
typedef ZoneUnaryCallback<R, T> = R Function(T);

final class ErrorResult implements Result<Never> {
  final Object error;
  final StackTrace? _stackTrace;

  const ErrorResult(this.error, [this._stackTrace]);

  @override
  ErrorResult get asError => this;

  @override
  Future<Never> get asFuture => Future<Never>.error(error, _stackTrace);

  @override
  ValueResult<Never>? get asValue => null;

  @override
  int get hashCode => error.hashCode ^ _stackTrace.hashCode ^ 0x1d61823f;

  @override
  bool get isError => true;

  @override
  bool get isValue => false;

  StackTrace get stackTrace {
    return _stackTrace ?? AsyncError.defaultStackTrace(error);
  }

  @override
  bool operator ==(Object other) =>
      other is ErrorResult &&
      error == other.error &&
      _stackTrace == other._stackTrace;

  void handle(Function errorHandler) {
    if (errorHandler is ZoneBinaryCallback) {
      errorHandler(error, _stackTrace);
    } else {
      (errorHandler as ZoneUnaryCallback)(error);
    }
  }

  @override
  String toString() {
    if (!AsyncLiteConfig.printLog) return '$runtimeType';
    return 'ErrorResult{error: $error, stackTrace: $stackTrace}';
  }
}
