part of '../async_lite.dart';

class NotNullableError<T> extends Error implements TypeError {
  final String _name;

  NotNullableError(this._name);

  @override
  String toString() {
    if (!AsyncLiteConfig.printLog) return '$runtimeType';
    return "Null is not a valid value for '$_name' of type '$T'";
  }
}
