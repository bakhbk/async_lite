import 'package:async_lite/async_lite.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test(
      'async test, checking a function with multiple function '
      'calls and getting the result of the first call', () async {
    for (var i = 0; i <= 4; i++) {
      _getIntValue.byLazy().then((result) {
        expect(result, 1);
        expect(result, isA<int>());
      });
      _getStringValue.byLazy().then((result) {
        expect(result, 'completed 1 times');
        expect(result, isA<String>());
      });
    }
    await Future<void>.delayed(const Duration(seconds: 3));
    for (var i = 0; i <= 4; i++) {
      _getIntValue.byLazy().then((result) {
        expect(result, 2);
        expect(result, isA<int>());
      });
      _getStringValue.byLazy().then((result) {
        expect(result, 'completed 2 times');
        expect(result, isA<String>());
      });
    }
    await Future<void>.delayed(const Duration(seconds: 3));
  });
}

int _value = 0;
Future<int> _getIntValue() async {
  await Future<void>.delayed(const Duration(seconds: 2));
  return ++_value;
}

Future<String> _getStringValue() async {
  await Future<void>.delayed(const Duration(seconds: 2));
  return 'completed $_value times';
}
