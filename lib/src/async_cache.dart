part of '../async_lite.dart';

class AsyncCache<T> {
  final Duration? _duration;
  Future<T>? _cachedValueFuture;

  Timer? _stale;

  AsyncCache(Duration duration) : _duration = duration;

  AsyncCache.ephemeral() : _duration = null;

  Future<T> fetch(Future<T> Function() callback) async {
    return _cachedValueFuture ??= callback()
      ..whenComplete(_startStaleTimer).ignore();
  }

  void invalidate() {
    _cachedValueFuture = null;
    _stale?.cancel();
    _stale = null;
  }

  @override
  String toString() {
    if (!AsyncLiteConfig.printLog) return '$runtimeType';
    return 'AsyncCache{_duration: $_duration, _cachedValueFuture: $_cachedValueFuture, _stale: $_stale}';
  }

  void _startStaleTimer() {
    final duration = _duration;
    if (duration != null) {
      _stale = Timer(duration, invalidate);
    } else {
      invalidate();
    }
  }
}
