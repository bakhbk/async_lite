part of '../async_lite.dart';

extension LazyFutureExt<T> on Future<T> Function() {
  static final _map = <Object, AsyncCache>{};

  Future<T> byLazy([Object? key]) {
    final aKey = key ?? toString();
    final asyncCache = _map[aKey] ??= AsyncCache<T>.ephemeral();
    return asyncCache.fetch(this).then((result) {
      return result as T;
    }).whenComplete(() {
      clearByKey(aKey);
    });
  }

  static Future<T> callByLazy<T>(Future<T> Function() call, {Object? key}) {
    return call.byLazy(key);
  }

  static void clearByKey(Object key) {
    if (_map.containsKey(key)) {
      _map[key]!.invalidate();
      _map.remove(key);
    }
  }

  static void clearCacheMap() {
    for (final e in _map.values) {
      e.invalidate();
    }
    _map.clear();
  }
}
