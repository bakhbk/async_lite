# Lightweight version of the popular package

- uses the Result class and inheritors (ValueResult, Error Result)
- the "LazyFutureExt" extension allows you to get the result of the first call when calling an asynchronous function multiple times
