part of '../async_lite.dart';

final class AsyncError implements Error {
  final Object error;
  @override
  final StackTrace stackTrace;

  AsyncError(Object error, StackTrace? stackTrace)
      : error = checkNotNullable(error, "error"),
        stackTrace = stackTrace ?? defaultStackTrace(error);

  @override
  String toString() {
    if (!AsyncLiteConfig.printLog) return '$runtimeType';
    return 'AsyncError{error: $error, stackTrace: $stackTrace}';
  }

  static StackTrace defaultStackTrace(Object error) {
    if (error is Error) {
      final stackTrace = error.stackTrace;
      if (stackTrace != null) return stackTrace;
    }
    return StackTrace.empty;
  }
}
