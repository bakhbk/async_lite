part of '../../async_lite.dart';

sealed class Result<T> {
  factory Result(T Function() computation) {
    try {
      return ValueResult<T>(computation());
    } on Object catch (e, s) {
      return ErrorResult(e, s);
    }
  }

  factory Result.value(T value) = ValueResult<T>;

  factory Result.error(Object error, [StackTrace? stackTrace]) = ErrorResult;

  static Future<Result<T>> capture<T>(Future<T> future) {
    return future.then(ValueResult.new, onError: ErrorResult.new);
  }

  static Future<T> release<T>(Future<Result<T>> future) =>
      future.then<T>((result) => result.asFuture);

  static Result<T> flatten<T>(Result<Result<T>> result) {
    return switch (result) {
      ValueResult() => result.value,
      ErrorResult() => result,
    };
  }

  static Result<List<T>> flattenAll<T>(Iterable<Result<T>> results) {
    final values = <T>[];
    for (final result in results) {
      switch (result) {
        case ValueResult<T>():
          values.add(result.value);
        case ErrorResult():
          return result;
      }
    }
    return Result<List<T>>.value(values);
  }

  bool get isValue;

  bool get isError;

  ValueResult<T>? get asValue;

  ErrorResult? get asError;

  Future<T> get asFuture;
}
